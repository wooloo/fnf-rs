use std::cmp::Ordering;

use hell_cue_format::{Cue, HellCues};
use rayon::iter::IntoParallelRefIterator;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Chart {
	pub song: Song,
	#[serde(default)]
	pub bpm: u32,
	#[serde(default)]
	pub sections: usize,
	#[serde(default)]
	pub notes: Vec<Section>,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Song {
	song: String,
	#[serde(default)]
	bpm: u32,
	#[serde(default)]
	sections: usize,
	notes: Vec<Section>,
	#[serde(rename = "needsVoices")]
	needs_voices: bool,
	speed: f64,
	player1: String,
	player2: String,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Section {
	#[serde(rename = "mustHitSection")]
	pub must_hit_section: bool,
	#[serde(default)]
	#[serde(rename = "typeOfSection")]
	pub type_of_section: u8,
	#[serde(rename = "lengthInSteps")]
	pub length_in_steps: usize,
	#[serde(rename = "sectionNotes")]
	pub section_notes: Vec<Note>,
	#[serde(rename = "altAnim")]
	#[serde(default)]
	pub use_alternate_animation: bool,
	pub bpm: Option<u32>,
	#[serde(rename = "changeBPM")]
	#[serde(default)]
	pub change_bpm: bool,
}

/// The start of the note in milliseconds, the key it's on, and its length?
type Note = (usize, u8, f64);

pub struct ChartConfig {
	pub bpm: u32,
	pub song: String,
	pub needs_voices: bool,
	pub speed: f64,
	pub player1: String,
	pub player2: String,
}
impl Default for ChartConfig {
	fn default() -> Self {
		ChartConfig {
			bpm: 120,
			song: "Tutorial".to_string(),
			needs_voices: false,
			speed: 1.0,
			player1: "bf".to_string(),
			player2: "gf".to_string(),
		}
	}
}
impl Default for Section {
	fn default() -> Self {
		Section {
			must_hit_section: true,
			type_of_section: 0,
			length_in_steps: 0,
			section_notes: vec![],
			use_alternate_animation: false,
			bpm: Some(120),
			change_bpm: false,
		}
	}
}

impl Into<Chart> for (HellCues, ChartConfig) {
	fn into(self) -> Chart {
		let (hell_cues, cfg) = self;

		assert_eq!(hell_cues.game_id, "fnf");
		let sections: Vec<Section> = Section::from_cues(&cfg, &hell_cues.cues, "player", "enemy");
		// let player_sections = sections
		// 	.iter()
		// 	.filter(|v| v.must_hit_section)
		// 	.map(|v| v.clone())
		// 	.collect::<Vec<Section>>();
		// let enemy_sections = sections
		// 	.iter()
		// 	.filter(|v| !v.must_hit_section)
		// 	.map(|v| v.clone())
		// 	.collect::<Vec<Section>>();
		Chart {
			song: Song {
				song: cfg.song,
				bpm: cfg.bpm,
				sections: sections.len(),
				notes: sections.clone(),
				needs_voices: cfg.needs_voices,
				speed: cfg.speed,
				player1: cfg.player1,
				player2: cfg.player2,
			},
			bpm: std::env::var("BPM")
				.unwrap_or("120".to_string())
				.parse()
				.unwrap(),
			sections: sections.len(),
			notes: sections.clone(),
		}
	}
}
impl Section {
	fn from_cues(cfg: &ChartConfig, cues: &Vec<Cue>, player: &str, enemy: &str) -> Vec<Section> {
		let mut cues = cues
			.iter()
			.filter(|v| v.instrument == player || v.instrument == enemy)
			.collect::<Vec<&Cue>>();
		let lowest_note = {
			let mut lowest = u32::MAX;
			let mut highest = u32::MIN;
			for cue in cues.clone() {
				if lowest > cue.note {
					lowest = cue.note
				}
				if highest < cue.note {
					highest = cue.note
				}
			}
			if highest - lowest > 5 {
				eprintln!("Warning: Notes are present outside the normal range.");
			}
			lowest
		};
		let section = lowest_note;
		let base = lowest_note + 1;
		cues.sort_by(|a, b| match a.time.partial_cmp(&b.time).unwrap() {
			Ordering::Equal => {
				if a.note == section {
					return Ordering::Less;
				}
				if b.note == section {
					return Ordering::Greater;
				}
				Ordering::Equal
			}
			v => v,
		});
		let mut output = vec![];
		let mut current_player = Section::default();
		let mut timer_player = 0.0;
		let mut current_enemy = Section::default();
		let mut timer_enemy = 0.0;
		// this is disgusting, it splits each note into its own section with absurd
		// length values, but it was the only way i could keep the game from
		// crashing 20 seconds into the song
		for cue in cues.clone() {
			let is_player = cue.instrument == player;
			let (current, current_timer) = if is_player {
				(&mut current_player, &mut timer_player)
			} else {
				(&mut current_enemy, &mut timer_enemy)
			};

			current.must_hit_section = is_player;

			*current_timer = cue.time;
			match cue.note {
				v if v == section => {}
				note => {
					let time = *current_timer * 1000.0;
					let note = note - base;
					let length = 0.0;
					current
						.section_notes
						.push((time as usize, note as u8, length));
				}
			}
			current.length_in_steps = 100;
			output.push(current.clone());
			*current = Section::default();
			current.bpm = None;
		}
		output.push(current_player);
		output.push(current_enemy);
		output
	}
}
