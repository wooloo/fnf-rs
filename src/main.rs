use crate::lib::{Chart, ChartConfig};
use hell_cue_format::HellCues;
use std::io::Read;

mod lib;

fn main() {
	let mut input = vec![];
	std::io::stdin()
		.read_to_end(&mut input)
		.expect("Error reading stdin");
	let hell_cues = serde_json::from_slice::<HellCues>(input.as_slice()).unwrap();
	let mut cfg = ChartConfig::default();
	cfg.speed = 1.5;
	cfg.needs_voices = false;
	cfg.bpm = 180;
	cfg.song = "Bopeebo".to_string();
	cfg.player2 = "dad".to_string();
	let chart: Chart = (hell_cues, cfg).into();
	let chart = serde_json::to_string(&chart).unwrap();
	print!("{}", chart)
}
