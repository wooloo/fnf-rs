use std::fs;

use fnf::Chart;
use rayon::prelude::*;
use serde_json::Result;

const FILES: &'static str = include_str!("files");

#[test]
fn test_all_charts() {
	let results = FILES
		.split("\n")
		.par_bridge()
		.filter(|v| v.len() > 0)
		.map(|path| {
			(
				match fs::read_to_string(path) {
					Ok(v) => v,
					Err(e) => panic!("Reading {} failed with error {}", path, e),
				},
				path,
			)
		})
		.map(|data| (serde_json::from_str::<Chart>(&data.0), data.1))
		.collect::<Vec<_>>()
		.iter()
		.for_each(|v: &(Result<Chart>, &str)| {
			println!("{}", v.1);
			v.0.as_ref().unwrap();
		});
}
