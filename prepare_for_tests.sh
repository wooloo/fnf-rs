#!/bin/bash
echo "Preparing a copy of FNF's source code to ensure our schema is accurate."
git clone https://github.com/ninjamuffin99/Funkin Funkin || (cd Funkin && git pull)
find Funkin/assets/preload/data -name \*.json -not -name smash.json >tests/files
